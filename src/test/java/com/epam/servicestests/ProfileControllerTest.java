package com.epam.servicestests;


import com.epam.models.ProfileModel;
import com.epam.models.submodels.AddressModel;
import com.epam.servicestests.dataproviders.DataProviders;
import com.epam.utils.Constants;
import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProfileControllerTest extends BaseTest{

    @Test
    void getProfileTest() {
        String responseString = RestAssured.given()
                .cookies(cookies)
                .contentType(ContentType.JSON)
                .when()
                .get(Constants.PROFILE_PATH)
                .then()
                .statusCode(200).extract().response().getBody().asString();
        Assert.assertTrue(responseString.contains("\"email\":" + "\"" + Constants.TEST_EMAIL + "\""));
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "saveProfileData")
    void saveProfileTest(String country, String city, String Street, String streetNumber, String latitude, String longitude,
                         String dateOfBirth, String firstName, String secondName, String gender, String fileName, String responseCode) {
        AddressModel addressModel = new AddressModel()
                .setCity(city)
                .setCountry(country)
                .setStreet(Street)
                .setStreetNumber(streetNumber)
                .setLatitude(Double.valueOf(latitude))
                .setLongitude(Double.valueOf(longitude));
        ProfileModel testProfile = new ProfileModel()
                .setAddress(addressModel)
                .setDateOfBirthday(dateOfBirth)
                .setEmail(Constants.TEST_EMAIL)
                .setFirstName(firstName)
                .setSecondName(secondName)
                .setGender(gender)
                .setFileName(fileName);
        String responseLine = RestAssured.given()
                .cookies(cookies)
                .contentType(ContentType.JSON)
                .body(testProfile.getInJSON())
                .post(Constants.PROFILE_PATH)
                .then()
                .statusCode(Integer.valueOf(responseCode)).extract().response().getBody().asString();
        Gson gson = new Gson();
        ProfileModel response = gson.fromJson(responseLine, ProfileModel.class);
        Assert.assertEquals(testProfile, response);
    }
}
