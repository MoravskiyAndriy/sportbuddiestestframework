package com.epam.servicestests;

import com.epam.utils.Constants;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Cookies;
import org.testng.annotations.BeforeClass;

import static io.restassured.RestAssured.given;

public class BaseTest {
    protected Cookies cookies;

    @BeforeClass
    void loginAndInitialize() {
        RestAssured.baseURI = Constants.BASE_PATH;
        cookies = given().contentType(ContentType.JSON)
                .body(Constants.TEST_LOGIN_REQUEST_DATA)
                .post(Constants.LOGIN_PATH)
                .then().statusCode(200)
                .extract()
                .response()
                .getDetailedCookies();
    }
}
