package com.epam.servicestests.dataproviders;

import com.epam.dataparcer.CSVDataParser;
import com.epam.utils.Constants;
import org.testng.annotations.DataProvider;

import java.util.Arrays;
import java.util.Iterator;

public class DataProviders {
    private static final String PROFILE_TEST_DATAFILE_PATH = "./src/test/resources/profileTestData.csv";
    private static final String REQUEST_TEST_DATAFILE_PATH = "./src/test/resources/requestTestData.csv";

    private DataProviders() {
    }

    @DataProvider
    public static Iterator<Object[]> getRequestByIdData() {
        return Arrays.stream(new Object[][]{{211, Constants.SUCCESS},
                {Integer.MAX_VALUE, Constants.NOT_FOUND}}).iterator();
    }

    @DataProvider
    public static Iterator<Object[]> editRequestByIdData() {
        return Arrays.stream(new Object[][]{{Constants.REQUEST_ID_FOR_EDITING, Constants.SUCCESS},
                {Integer.MAX_VALUE, Constants.NOT_FOUND}}).iterator();
    }

    @DataProvider
    public static Iterator<Object[]> saveProfileData() {
        return CSVDataParser.readFile(PROFILE_TEST_DATAFILE_PATH);
    }

    @DataProvider
    public static Iterator<Object[]> findPartnersData() {
        return CSVDataParser.readFile(REQUEST_TEST_DATAFILE_PATH);
    }
}
