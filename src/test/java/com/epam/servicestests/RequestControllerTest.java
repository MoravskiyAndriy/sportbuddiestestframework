package com.epam.servicestests;

import com.epam.models.RequestModel;
import com.epam.models.ResponseModel;
import com.epam.servicestests.dataproviders.DataProviders;
import com.epam.utils.Constants;
import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class RequestControllerTest extends BaseTest{
    private static final String CREATE_REQUEST_EXTRA_URI_PART = "/1";
    private Integer createdRequestId = 0;

    @Test
    void getRequestsListTest() {
        String responseString = RestAssured.given()
                .cookies(cookies)
                .contentType(ContentType.JSON)
                .when()
                .get(Constants.REQUESTS_PATH)
                .then()
                .statusCode(200).extract().response().getBody().asString();
        for (String id : Constants.GUARANTEED_REQUEST_IDS) {
            Assert.assertTrue(responseString.contains("\"id\":" + id));
        }
    }

    @Test
    void addRequestTest() {
        RequestModel testRequest = getTestRequestModel();
        String responseLine = RestAssured.given()
                .cookies(cookies)
                .post(Constants.REQUESTS_PATH + CREATE_REQUEST_EXTRA_URI_PART + testRequest.toString())
                .then()
                .statusCode(200).extract().response().getBody().asString();
        Assert.assertTrue(responseLine.contains(Constants.SUCCESS));
        Gson gson = new Gson();
        ResponseModel response = gson.fromJson(responseLine, ResponseModel.class);
        createdRequestId = Integer.valueOf(response.getMessage().replaceAll("\\D+", ""));
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "getRequestByIdData")
    void getRequestByIdTest(int id, String responseMessage) {
        String response = RestAssured.given()
                .cookies(cookies)
                .get(Constants.REQUESTS_PATH + "/" + id)
                .then()
                .statusCode(200).extract().response().getBody().asString();
        Assert.assertTrue(response.contains(responseMessage));
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "editRequestByIdData")
    void editRequestByIdTest(int id, String responseMessage) {
        RequestModel testRequest = getTestRequestModel();
        String response = RestAssured.given()
                .cookies(cookies)
                .put(Constants.REQUESTS_PATH + "/" + id + testRequest.toString())
                .then()
                .statusCode(200).extract().response().getBody().asString();
        Assert.assertTrue(response.contains(responseMessage));
    }

    @Test
    void deleteLastRequest() {
        String badIdResponse = getDeleteByIdResponse(Integer.MAX_VALUE)
                .then()
                .statusCode(200).extract().response().getBody().asString();
        Assert.assertTrue(badIdResponse.contains(Constants.NOT_FOUND));

        RequestModel testRequest = getTestRequestModel();
        String responseLine = RestAssured.given()
                .cookies(cookies)
                .post(Constants.REQUESTS_PATH + CREATE_REQUEST_EXTRA_URI_PART + testRequest.toString())
                .then()
                .statusCode(200).extract().response().getBody().asString();
        Assert.assertTrue(responseLine.contains(Constants.SUCCESS));

        Gson gson = new Gson();
        ResponseModel responseModel = gson.fromJson(responseLine, ResponseModel.class);
        Integer newlyCreatedRequestId = Integer.valueOf(responseModel.getMessage().replaceAll("\\D+", ""));
        String validIdResponse = getDeleteByIdResponse(newlyCreatedRequestId)
                .then()
                .statusCode(200).extract().response().getBody().asString();
        Assert.assertTrue(validIdResponse.contains(Constants.SUCCESS));
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "findPartnersData")
    void findPartnersIdTest(String distance, String minRunningDistance, String maxRunningDistance,
                            String morning, String afternoon, String evening,
                            String gender, String maturityLevel, String sportType, String responseCode, String responseMessage) {
        RequestModel testRequest = new RequestModel()
                .setAfternoon(afternoon.equals("") ? null : Boolean.valueOf(afternoon))
                .setDistance(distance.equals("") ? null : Integer.valueOf(distance))
                .setEvening(evening.equals("") ? null : Boolean.valueOf(evening))
                .setGender(gender.equals("") ? null : Gender.valueOf(gender))
                .setMaturityLevel(maturityLevel.equals("") ? null : MaturityLevel.valueOf(maturityLevel))
                .setMorning(morning.equals("") ? null : Boolean.valueOf(morning))
                .setMinRunningDistance(minRunningDistance.equals("") ? null : Integer.valueOf(minRunningDistance))
                .setMaxRunningDistance(maxRunningDistance.equals("") ? null : Integer.valueOf(maxRunningDistance))
                .setSportType(sportType.equals("") ? null : SportType.valueOf(sportType));
        String responseBody = RestAssured.given()
                .cookies(cookies)
                .get(Constants.FIND_PARTNERS_PATH+testRequest.toString())
                .then()
                .statusCode(Integer.valueOf(responseCode)).extract().response().getBody().asString();
        Assert.assertTrue(responseBody.contains(responseMessage));
    }

    @AfterClass
    void clean() {
        RestAssured.given()
                .cookies(cookies)
                .delete(Constants.REQUESTS_PATH + "/" + createdRequestId);
    }

    private RequestModel getTestRequestModel() {
        return new RequestModel()
                .setDistance(10)
                .setGender(Gender.FEMALE)
                .setMaturityLevel(MaturityLevel.MIDDLE)
                .setMorning(false)
                .setAfternoon(true)
                .setEvening(true)
                .setMinRunningDistance(1)
                .setMaxRunningDistance(11)
                .setSportType(SportType.RUNNING);
    }

    private Response getDeleteByIdResponse(Integer id) {
        return RestAssured.given()
                .cookies(cookies)
                .delete(Constants.REQUESTS_PATH + "/" + id);
    }
}
