package com.epam.utils;

public class Constants {
    public static final String[] GUARANTEED_REQUEST_IDS = new String[]{"211", "212"};
    public static final String BASE_PATH = "https://sportbuddies.herokuapp.com/";
    public static final String TEST_EMAIL = "SeleniumWebzxcTest1@gmail.com";
    public static final String TEST_PASSWORD = "seleniumwebzxctest1";
    public static final String REQUESTS_PATH = "/requests";
    public static final String LOGIN_PATH = "auth/login";
    public static final String PROFILE_PATH = "/profile";
    public static final String FIND_PARTNERS_PATH = "/requests/find-partners";
    public static final Integer REQUEST_ID_FOR_EDITING = 214;
    public static final String SUCCESS = "SUCCESS";
    public static final String NOT_FOUND = "NOT_FOUND";
    public static final String TEST_LOGIN_REQUEST_DATA = "{\n" +
            "  \"email\": \""+TEST_EMAIL+"\",\n" +
            "  \"password\": \""+TEST_PASSWORD+"\",\n" +
            "  \"passwordConfirm\": \""+TEST_PASSWORD+"\",\n" +
            "  \"token\": \"\"\n" +
            "}";


    private Constants() {
    }
}
