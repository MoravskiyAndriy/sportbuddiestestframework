package com.epam.utils.enums;

public enum SportType {
    WALKING, RUNNING, SWIMMING, CYCLING, TENNIS, FOOTBALL, YOGA, GYM_TRAINING
}
