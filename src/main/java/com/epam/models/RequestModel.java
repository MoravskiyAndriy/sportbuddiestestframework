package com.epam.models;

import com.epam.utils.enums.Gender;
import com.epam.utils.enums.MaturityLevel;
import com.epam.utils.enums.SportType;
import org.json.simple.JSONObject;

import java.util.Objects;

public class RequestModel {
    private Boolean morning;
    private Boolean afternoon;
    private Boolean evening;
    private Integer distance;
    private Gender gender;
    private Integer id;
    private MaturityLevel maturityLevel;
    private Integer minRunningDistance;
    private Integer maxRunningDistance;
    private SportType sportType;

    public RequestModel() {
    }

    public Boolean isMorning() {
        return morning;
    }

    public RequestModel setMorning(Boolean morning) {
        this.morning = morning;
        return this;
    }

    public Boolean isAfternoon() {
        return afternoon;
    }

    public RequestModel setAfternoon(Boolean afternoon) {
        this.afternoon = afternoon;
        return this;
    }

    public Boolean isEvening() {
        return evening;
    }

    public RequestModel setEvening(Boolean evening) {
        this.evening = evening;
        return this;
    }

    public Integer getDistance() {
        return distance;
    }

    public RequestModel setDistance(Integer distance) {
        this.distance = distance;
        return this;
    }

    public Gender getGender() {
        return gender;
    }

    public RequestModel setGender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public RequestModel setId(Integer id) {
        this.id = id;
        return this;
    }

    public MaturityLevel getMaturityLevel() {
        return maturityLevel;
    }

    public RequestModel setMaturityLevel(MaturityLevel maturityLevel) {
        this.maturityLevel = maturityLevel;
        return this;
    }

    public Integer getMinRunningDistance() {
        return minRunningDistance;
    }

    public RequestModel setMinRunningDistance(Integer minRunningDistance) {
        this.minRunningDistance = minRunningDistance;
        return this;
    }

    public Integer getMaxRunningDistance() {
        return maxRunningDistance;
    }

    public RequestModel setMaxRunningDistance(Integer maxRunningDistance) {
        this.maxRunningDistance = maxRunningDistance;
        return this;
    }

    public SportType getSportType() {
        return sportType;
    }

    public RequestModel setSportType(SportType sportType) {
        this.sportType = sportType;
        return this;
    }

    @Override
    public String toString() {
        return "?afternoon=" + (Objects.isNull(afternoon) ? "": afternoon) +
                "&distance=" + (Objects.isNull(distance) ? "": distance) +
                "&evening=" + (Objects.isNull(evening) ? "": evening) +
                "&gender=" +(Objects.isNull(gender) ? "":  gender) +
                "&maturityLevel=" + (Objects.isNull(maturityLevel) ? "": maturityLevel) +
                "&maxRunningDistance=" + (Objects.isNull(maxRunningDistance) ? "": maxRunningDistance) +
                "&minRunningDistance=" + (Objects.isNull(minRunningDistance) ? "": minRunningDistance) +
                "&morning=" + (Objects.isNull(morning) ? "": morning) +
                "&sportType=" + (Objects.isNull(sportType) ? "": sportType);
    }

    public JSONObject getInJSON() {
        JSONObject testRequest = new JSONObject();
        testRequest.put("afternoon", Objects.isNull(afternoon) ? "": afternoon);
        testRequest.put("distance", Objects.isNull(distance) ? "": distance);
        testRequest.put("evening", Objects.isNull(evening) ? "": evening);
        testRequest.put("gender", Objects.isNull(gender) ? "": gender);
        testRequest.put("maturityLevel", Objects.isNull(maturityLevel) ? "": maturityLevel);
        testRequest.put("maxRunningDistance", Objects.isNull(maxRunningDistance) ? "": maxRunningDistance);
        testRequest.put("minRunningDistance", Objects.isNull(minRunningDistance) ? "": minRunningDistance);
        testRequest.put("morning", Objects.isNull(morning) ? "": morning);
        testRequest.put("sportType", Objects.isNull(sportType) ? "": sportType);
        return testRequest;
    }
}
