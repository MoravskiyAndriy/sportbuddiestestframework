package com.epam.models;

import org.json.simple.JSONObject;

public class ResponseModel {
    private String status;
    private RequestModel payload;
    private String message;

    public ResponseModel(){}

    public String getStatus() {
        return status;
    }

    public ResponseModel setStatus(String status) {
        this.status = status;
        return this;
    }

    public RequestModel getPayload() {
        return payload;
    }

    public ResponseModel setPayload(RequestModel payload) {
        this.payload = payload;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseModel setMessage(String message) {
        this.message = message;
        return this;
    }

    public JSONObject getInJSON() {
        JSONObject testResponce = new JSONObject();
        testResponce.put("status", status);
        testResponce.put("payload", payload);
        testResponce.put("message", message);
        return testResponce;
    }

    @Override
    public String toString() {
        return "ResponseModel{" +
                "status='" + status + '\'' +
                ", payload=" + payload +
                ", message='" + message + '\'' +
                '}';
    }
}
