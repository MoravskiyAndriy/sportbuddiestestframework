package com.epam.models;

import com.epam.models.submodels.AddressModel;
import org.json.simple.JSONObject;

import java.util.Objects;

public class ProfileModel {
    private AddressModel address;
    private String dateOfBirthday;
    private String email;
    private String fileName;
    private String firstName;
    private String gender;
    private String secondName;

    public ProfileModel() {
    }

    public AddressModel getAddress() {
        return address;
    }

    public ProfileModel setAddress(AddressModel address) {
        this.address = address;
        return this;
    }

    public String getDateOfBirthday() {
        return dateOfBirthday;
    }

    public ProfileModel setDateOfBirthday(String dateOfBirthday) {
        this.dateOfBirthday = dateOfBirthday;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ProfileModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public ProfileModel setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public ProfileModel setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public ProfileModel setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getSecondName() {
        return secondName;
    }

    public ProfileModel setSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public JSONObject getInJSON() {
        JSONObject testProfile = new JSONObject();
        testProfile.put("address", address);
        testProfile.put("dateOfBirthday", dateOfBirthday);
        testProfile.put("email", email);
        testProfile.put("fileName", fileName);
        testProfile.put("firstName", firstName);
        testProfile.put("gender", gender);
        testProfile.put("secondName", secondName);
        return testProfile;
    }

    @Override
    public String toString() {
        return "ProfileModel{" +
                "address=" + address +
                ", dateOfBirthday='" + dateOfBirthday + '\'' +
                ", email='" + email + '\'' +
                ", fileName='" + fileName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", gender=" + gender +
                ", secondName='" + secondName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProfileModel)) return false;
        ProfileModel that = (ProfileModel) o;
        return Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getDateOfBirthday(), that.getDateOfBirthday()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getFileName(), that.getFileName()) &&
                Objects.equals(getFirstName(), that.getFirstName()) &&
                Objects.equals(getGender(), that.getGender()) &&
                Objects.equals(getSecondName(), that.getSecondName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAddress(), getDateOfBirthday(), getEmail(), getFileName(), getFirstName(), getGender(), getSecondName());
    }
}
