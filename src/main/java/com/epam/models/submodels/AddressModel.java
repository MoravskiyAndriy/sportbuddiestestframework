package com.epam.models.submodels;

import org.json.simple.JSONObject;

import java.util.Objects;

public class AddressModel {
    private String city;
    private String country;
    private Double latitude;
    private Double longitude;
    private String street;
    private String streetNumber;

    public AddressModel() {
    }

    public String getCity() {
        return city;
    }

    public AddressModel setCity(String city) {
        this.city = city;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public AddressModel setCountry(String country) {
        this.country = country;
        return this;
    }

    public Double getLatitude() {
        return latitude;
    }

    public AddressModel setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public AddressModel setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public AddressModel setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public AddressModel setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                "city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", street='" + street + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                '}';
    }

    public JSONObject getInJSON() {
        JSONObject testAdsress = new JSONObject();
        testAdsress.put("city", city);
        testAdsress.put("country", country);
        testAdsress.put("latitude", latitude);
        testAdsress.put("longitude", longitude);
        testAdsress.put("street", street);
        testAdsress.put("streetNumber", streetNumber);
        return testAdsress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressModel)) return false;
        AddressModel that = (AddressModel) o;
        return Objects.equals(getCity(), that.getCity()) &&
                Objects.equals(getCountry(), that.getCountry()) &&
                Objects.equals(getLatitude(), that.getLatitude()) &&
                Objects.equals(getLongitude(), that.getLongitude()) &&
                Objects.equals(getStreet(), that.getStreet()) &&
                Objects.equals(getStreetNumber(), that.getStreetNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCity(), getCountry(), getLatitude(), getLongitude(), getStreet(), getStreetNumber());
    }
}
